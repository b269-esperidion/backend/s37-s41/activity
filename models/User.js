const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		// "true" values defines if the field is required or not
		// "First name is required" value is the message that will be printed out in our terminal when data is not present
		required : [true, "First name is required"]},
	lastName : {
		type : String,
		required : [true, "Last name is required"]},
	email : {
		type : String,
		required : [true, "Email is required"]},
	password : {
		type : String,
		required : [true, "Password is required"]},
	isAdmin : {
		type : Boolean,
		default : false},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]},
	enrollments : [
		{
			courseId : {
				type : String,
				required : [true, "Course ID is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "Enrolled"
			}
		}]
});




/*ACTIVITY*/
const courseSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Name is required"]},
	description : {
		type : String,
		required : [true, "Description is required"]},
	price : {
		type : Number,
		required : [true, "Price is required"]},
	isActive : {
		type : String,
		required : [true, "Is active is required"]},
	isAdmin : {
		type : Boolean,
		default : false},
	createdOn : {
				type : Date,
				required : [true, "Date is required"]},
	enrollees : [{
			userId : {
				type : String,
				required : [true, "User ID is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date()
				required : [true, "Date is required"]
			}
		}]

});




module.exports = mongoose.model("User", userSchema);
module.exports = mongoose.model("User", courseSchema);
